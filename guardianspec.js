import TestConstant from 'TestConstant';
import RepoGuardian from 'RepoGuardian';
"use strict";
describe('Guardian Wesite ', function () {

    beforeAll(function () {
        browser.waitForAngularEnabled(false);
        browser.get(website);
    });
    it(' verify logo', function () {

        browser.driver.manage().window().maximize();
        expect(logo.isDisplayed()).toBe(true);
    });
    it(' Temperature of the place', function () {
        console.log('Temperature now: ')
        tempnow.getText().then(console.log);
    });
    it('Verify header links are present', function () {
        expect(supportGuardianLogo.isDisplayed()).toBe(true);
        expect(subscribeLink.isDisplayed()).toBe(true);
        expect(findAJob.isDisplayed()).toBe(true);
        expect(signIn.isDisplayed()).toBe(true);
        expect(search.isDisplayed()).toBe(true);
        expect(editionPicker.isDisplayed()).toBe(true);
    });
    it('Verify header links open the actual site ', function () {
        supportGuardianLogo.click();
        expect(otherAmount.isDisplayed()).toBe(true);
        browser.navigate().back();
        subscribeLink.click();
        expect(subscribeHeading.isDisplayed()).toBe(true);
        browser.navigate().back();
        findAJob.click();
        expect(jobWorthDoing.isDisplayed()).toBe(true);
        browser.navigate().back();
        signIn.click();
        expect(signInRegister.isDisplayed()).toBe(true);
        browser.navigate().back();
        search.click();
        expect(gSearch.isDisplayed()).toBe(true);
        editionPicker.click();
        expect(editorDrop.isDisplayed()).toBe(true);
    });

    it('Verify header links goes to expected page', function () {
        expect(supportGuardianLogo.isDisplayed()).toBe(true);
        expect(subscribeLink.isDisplayed()).toBe(true);
        expect(findAJob.isDisplayed()).toBe(true);
        expect(signIn.isDisplayed()).toBe(true);
        expect(search.isDisplayed()).toBe(true);
        expect(editionPicker.isDisplayed()).toBe(true);
    });
    it('Verify News Sections links are present', function () {
        expect(news.isDisplayed()).toBe(true);
        expect(opinion.isDisplayed()).toBe(true);
        expect(sport.isDisplayed()).toBe(true);
        expect(culture.isDisplayed()).toBe(true);
        expect(lifestyle.isDisplayed()).toBe(true);
        expect(more.isDisplayed()).toBe(true);
    });
    afterAll(function () {
        browser.waitForAngularEnabled(true);
    });
});





























