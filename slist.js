describe('List if element',function()
{
    var first=element(by.model('first'));
    var second=element(by.model('second'));
    var go=element(by.id('gobutton'));
    var result=element(by.className('ng-binding'));
    var history= element.all(by.repeater('result in memory'));
    beforeEach(function()
    {
     browser.get('http://juliemr.github.io/protractor-demo');
    });
   function add(a,b){

    first.sendKeys(a);
    second.sendKeys(b);
    go.click();

   }
it('check the history',function()
{
add(1,2);
add(2,3);
add(3,4);
expect(history.count()).toEqual(3);
add(5,3);
add(3,7);
expect(history.count()).toEqual(5);
});
});