"use strict";
var website = 'https://www.theguardian.com/international';
var logo = element(by.xpath('//*[@class="inline-the-guardian-logo__svg inline-logo__svg"]'));
var tempnow = element(by.xpath('//span[@class="weather__temp js-weather-temp"]'));
var supportGuardianLogo = element(by.xpath('//span[@class="top-bar__item--cta--text"]'));
var subscribeLink = element(by.xpath('//a[@class="top-bar__item hide-until-tablet js-subscribe js-acquisition-link"]'));
var findAJob = element(by.xpath('//span[@class="hide-until-tablet"]'));
var signIn = element(by.xpath('//a[@class="top-bar__item hide-until-desktop js-navigation-sign-in"]'));
var search = element(by.xpath('//a[@class="top-bar__item popup__toggle hide-until-desktop js-search-toggle js-toggle-ready"]'));
var editionPicker = element(by.xpath('//button[@id="edition-picker-toggle"]'));
var news = element(by.xpath('//a[@class="pillar-link pillar-link--News pillar-link--current-section"]'));
var opinion = element(by.xpath('//a[@class="pillar-link pillar-link--Opinion"]'));
var sport = element(by.xpath('//a[@class="pillar-link pillar-link--Sport"]'));
var culture = element(by.xpath('//a[@class="pillar-link pillar-link--Culture"]'));
var lifestyle = element(by.xpath('//a[@class="pillar-link pillar-link--Lifestyle"]'));
var more = element(by.xpath('//span[@class="pillar-link pillar-link--dropdown pillar-link--sections hide-until-desktop"]'));
var otherAmount = element(by.xpath('//input[@id="qa-payment-amount-input"]'));
var subscribeHeading = element(by.xpath('//h2[contains(text(),"Subscribe")]'));
var jobWorthDoing = element(by.xpath('//span[contains(text(),"Jobs worth doing")]'));
var signInRegister = element(by.xpath('//h1[@class="layout-header__title"]'));
var gSearch = element(by.xpath('//input[@id="gsc-i-id1"]'));
var editorDrop = element(by.xpath('//a[@class="dropdown-menu__title dropdown-menu__title--active"]'));