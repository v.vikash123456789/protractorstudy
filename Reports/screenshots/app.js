var app = angular.module('reportingApp', []);

app.controller('ScreenshotReportController', function ($scope) {
    $scope.searchSettings = Object.assign({
        description: '',
        allselected: true,
        passed: true,
        failed: true,
        pending: true,
        withLog: true
    }, {}); // enable customisation of search settings on first page hit

    var initialColumnSettings = undefined; // enable customisation of visible columns on first page hit
    if (initialColumnSettings) {
        if (initialColumnSettings.displayTime !== undefined) {
            // initial settings have be inverted because the html bindings are inverted (e.g. !ctrl.displayTime)
            this.displayTime = !initialColumnSettings.displayTime;
        }
        if (initialColumnSettings.displayBrowser !== undefined) {
            this.displayBrowser = !initialColumnSettings.displayBrowser; // same as above
        }
        if (initialColumnSettings.displaySessionId !== undefined) {
            this.displaySessionId = !initialColumnSettings.displaySessionId; // same as above
        }
        if (initialColumnSettings.displayOS !== undefined) {
            this.displayOS = !initialColumnSettings.displayOS; // same as above
        }
        if (initialColumnSettings.inlineScreenshots !== undefined) {
            this.inlineScreenshots = initialColumnSettings.inlineScreenshots; // this setting does not have to be inverted
        }

    }


    $scope.inlineScreenshots = false;
    this.showSmartStackTraceHighlight = true;

    this.chooseAllTypes = function () {
        var value = true;
        $scope.searchSettings.allselected = !$scope.searchSettings.allselected;
        if (!$scope.searchSettings.allselected) {
            value = false;
        }

        $scope.searchSettings.passed = value;
        $scope.searchSettings.failed = value;
        $scope.searchSettings.pending = value;
        $scope.searchSettings.withLog = value;
    };

    this.isValueAnArray = function (val) {
        return isValueAnArray(val);
    };

    this.getParent = function (str) {
        var arr = str.split('|');
        str = "";
        for (var i = arr.length - 2; i > 0; i--) {
            str += arr[i] + " > ";
        }
        return str.slice(0, -3);
    };

    this.getSpec = function (str) {
        return getSpec(str);
    };


    this.getShortDescription = function (str) {
        return str.split('|')[0];
    };

    this.convertTimestamp = function (timestamp) {
        var d = new Date(timestamp),
            yyyy = d.getFullYear(),
            mm = ('0' + (d.getMonth() + 1)).slice(-2),
            dd = ('0' + d.getDate()).slice(-2),
            hh = d.getHours(),
            h = hh,
            min = ('0' + d.getMinutes()).slice(-2),
            ampm = 'AM',
            time;

        if (hh > 12) {
            h = hh - 12;
            ampm = 'PM';
        } else if (hh === 12) {
            h = 12;
            ampm = 'PM';
        } else if (hh === 0) {
            h = 12;
        }

        // ie: 2013-02-18, 8:35 AM
        time = yyyy + '-' + mm + '-' + dd + ', ' + h + ':' + min + ' ' + ampm;

        return time;
    };


    this.round = function (number, roundVal) {
        return (parseFloat(number) / 1000).toFixed(roundVal);
    };


    this.passCount = function () {
        var passCount = 0;
        for (var i in this.results) {
            var result = this.results[i];
            if (result.passed) {
                passCount++;
            }
        }
        return passCount;
    };


    this.pendingCount = function () {
        var pendingCount = 0;
        for (var i in this.results) {
            var result = this.results[i];
            if (result.pending) {
                pendingCount++;
            }
        }
        return pendingCount;
    };


    this.failCount = function () {
        var failCount = 0;
        for (var i in this.results) {
            var result = this.results[i];
            if (!result.passed && !result.pending) {
                failCount++;
            }
        }
        return failCount;
    };

    this.passPerc = function () {
        return (this.passCount() / this.totalCount()) * 100;
    };
    this.pendingPerc = function () {
        return (this.pendingCount() / this.totalCount()) * 100;
    };
    this.failPerc = function () {
        return (this.failCount() / this.totalCount()) * 100;
    };
    this.totalCount = function () {
        return this.passCount() + this.failCount() + this.pendingCount();
    };

    this.applySmartHighlight = function (line) {
        if (this.showSmartStackTraceHighlight) {
            if (line.indexOf('node_modules') > -1) {
                return 'greyout';
            }
            if (line.indexOf('  at ') === -1) {
                return '';
            }

            return 'highlight';
        }
        return true;
    };


    var results = [
    {
        "description": "Open and enter and sum|sum feature",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "d585e5160169c5337dbb51c03bbaa58f",
        "instanceId": 8764,
        "browser": {
            "name": "chrome",
            "version": "68.0.3440.106"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1536841027518,
                "type": ""
            }
        ],
        "screenShotFile": "00cf004d-0086-00fa-00f2-00cc00390039.png",
        "timestamp": 1536841024443,
        "duration": 9430
    },
    {
        "description": "title verify|get title",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "d585e5160169c5337dbb51c03bbaa58f",
        "instanceId": 8764,
        "browser": {
            "name": "chrome",
            "version": "68.0.3440.106"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00ed0037-00d6-00fe-0021-00bf001900be.png",
        "timestamp": 1536841035224,
        "duration": 93
    },
    {
        "description": "title verify|Testing juliemr.github.io",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "d585e5160169c5337dbb51c03bbaa58f",
        "instanceId": 8764,
        "browser": {
            "name": "chrome",
            "version": "68.0.3440.106"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00420090-0095-00df-00d3-00f1005500f4.png",
        "timestamp": 1536841035821,
        "duration": 1167
    },
    {
        "description": "addition|Testing juliemr.github.io",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "d585e5160169c5337dbb51c03bbaa58f",
        "instanceId": 8764,
        "browser": {
            "name": "chrome",
            "version": "68.0.3440.106"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00f700c1-0005-009a-0098-005d00730052.png",
        "timestamp": 1536841037551,
        "duration": 4948
    },
    {
        "description": "Open and enter and sum|sum feature",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "f14f9d936c11894450363eef267d2b1d",
        "instanceId": 4596,
        "browser": {
            "name": "chrome",
            "version": "68.0.3440.106"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1536841294280,
                "type": ""
            }
        ],
        "screenShotFile": "006100ee-0077-00cf-0041-00f1009f00c8.png",
        "timestamp": 1536841292854,
        "duration": 5456
    },
    {
        "description": "title verify|get title",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "f14f9d936c11894450363eef267d2b1d",
        "instanceId": 4596,
        "browser": {
            "name": "chrome",
            "version": "68.0.3440.106"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "003f001b-0067-00a5-0049-00e300d3009d.png",
        "timestamp": 1536841299397,
        "duration": 118
    },
    {
        "description": "title verify|Testing juliemr.github.io",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "f14f9d936c11894450363eef267d2b1d",
        "instanceId": 4596,
        "browser": {
            "name": "chrome",
            "version": "68.0.3440.106"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00270069-00d7-0020-00b3-00d3009e0077.png",
        "timestamp": 1536841299958,
        "duration": 602
    },
    {
        "description": "addition|Testing juliemr.github.io",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "f14f9d936c11894450363eef267d2b1d",
        "instanceId": 4596,
        "browser": {
            "name": "chrome",
            "version": "68.0.3440.106"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00a800e3-00bf-0030-0068-0092007700f6.png",
        "timestamp": 1536841301004,
        "duration": 3695
    },
    {
        "description": "Open and enter and sum|sum feature",
        "passed": false,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "804e327821ce3057ab538e69b4b86478",
        "instanceId": 9804,
        "browser": {
            "name": "chrome",
            "version": "68.0.3440.106"
        },
        "message": [
            "Expected '6' to equal '7'."
        ],
        "trace": [
            "Error: Failed expectation\n    at UserContext.<anonymous> (C:\\Users\\Vikash Khandelwal\\Documents\\protractorStudy\\spec.js:9:55)\n    at C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasminewd2\\index.js:112:25\n    at new ManagedPromise (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:1077:7)\n    at ControlFlow.promise (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:2505:12)\n    at schedulerExecute (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasminewd2\\index.js:95:18)\n    at TaskQueue.execute_ (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:3084:14)\n    at TaskQueue.executeNext_ (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:3067:27)\n    at asyncRun (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:2974:25)\n    at C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:668:7"
        ],
        "browserLogs": [
            {
                "level": "SEVERE",
                "message": "http://juliemr.github.io/favicon.ico - Failed to load resource: the server responded with a status of 404 (Not Found)",
                "timestamp": 1536841518093,
                "type": ""
            }
        ],
        "screenShotFile": "001f00f7-00d9-0088-00b2-000d00e7003b.png",
        "timestamp": 1536841516819,
        "duration": 5250
    },
    {
        "description": "title verify|get title",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "804e327821ce3057ab538e69b4b86478",
        "instanceId": 9804,
        "browser": {
            "name": "chrome",
            "version": "68.0.3440.106"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00ed0003-00fa-001e-0048-000b00af00be.png",
        "timestamp": 1536841522924,
        "duration": 63
    },
    {
        "description": "title verify|Testing juliemr.github.io",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "804e327821ce3057ab538e69b4b86478",
        "instanceId": 9804,
        "browser": {
            "name": "chrome",
            "version": "68.0.3440.106"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00a800ac-001a-0062-002d-00d100b200cf.png",
        "timestamp": 1536841523400,
        "duration": 682
    },
    {
        "description": "addition|Testing juliemr.github.io",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "804e327821ce3057ab538e69b4b86478",
        "instanceId": 9804,
        "browser": {
            "name": "chrome",
            "version": "68.0.3440.106"
        },
        "message": "Passed.",
        "trace": "",
        "browserLogs": [],
        "screenShotFile": "00f000fd-0036-000b-00e7-00c900da00ca.png",
        "timestamp": 1536841524521,
        "duration": 3308
    },
    {
        "description": "Angular non js automation test pratice|Just testing non angular app",
        "passed": false,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "0f549a523e11d4533b54ae0ce838ac05",
        "instanceId": 2416,
        "browser": {
            "name": "chrome",
            "version": "68.0.3440.106"
        },
        "message": [
            "Failed: Angular could not be found on the page http://toolsqa.com/automation-practice-form/. If this is not an Angular application, you may need to turn off waiting for Angular.\n                          Please see \n                          https://github.com/angular/protractor/blob/master/docs/timeouts.md#waiting-for-angular-on-page-load"
        ],
        "trace": [
            "Error: Angular could not be found on the page http://toolsqa.com/automation-practice-form/. If this is not an Angular application, you may need to turn off waiting for Angular.\n                          Please see \n                          https://github.com/angular/protractor/blob/master/docs/timeouts.md#waiting-for-angular-on-page-load\n    at executeAsyncScript_.then (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\built\\browser.js:720:27)\n    at ManagedPromise.invokeCallback_ (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:1376:14)\n    at TaskQueue.execute_ (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:3084:14)\n    at TaskQueue.executeNext_ (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:3067:27)\n    at asyncRun (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:2927:27)\n    at C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:668:7\n    at <anonymous>\n    at process._tickCallback (internal/process/next_tick.js:188:7)\nFrom: Task: Run it(\"Angular non js automation test pratice\") in control flow\n    at UserContext.<anonymous> (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasminewd2\\index.js:94:19)\n    at attempt (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4297:26)\n    at QueueRunner.run (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4217:20)\n    at runNext (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4257:20)\n    at C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4264:13\n    at C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4172:9\n    at C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasminewd2\\index.js:64:48\n    at ControlFlow.emit (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\events.js:62:21)\n    at ControlFlow.shutdown_ (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:2674:10)\n    at shutdownTask_.MicroTask (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\selenium-webdriver\\lib\\promise.js:2599:53)\nFrom asynchronous test: \nError\n    at Suite.<anonymous> (C:\\Users\\Vikash Khandelwal\\Documents\\protractorStudy\\nonangular.js:2:1)\n    at addSpecsToSuite (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:1107:25)\n    at Env.describe (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:1074:7)\n    at describe (C:\\Users\\Vikash Khandelwal\\AppData\\Roaming\\npm\\node_modules\\protractor\\node_modules\\jasmine-core\\lib\\jasmine-core\\jasmine.js:4399:18)\n    at Object.<anonymous> (C:\\Users\\Vikash Khandelwal\\Documents\\protractorStudy\\nonangular.js:1:63)\n    at Module._compile (module.js:652:30)\n    at Object.Module._extensions..js (module.js:663:10)\n    at Module.load (module.js:565:32)\n    at tryModuleLoad (module.js:505:12)"
        ],
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://code.jquery.com/jquery-1.10.2.js 8671 Synchronous XMLHttpRequest on the main thread is deprecated because of its detrimental effects to the end user's experience. For more help, check https://xhr.spec.whatwg.org/.",
                "timestamp": 1536845290140,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://20tvni1sjxyh352kld2lslvc.wpengine.netdna-cdn.com/wp-content/themes/dt-the7/js/above-the-fold.min.js?ver=3.8.1 0:10423 Uncaught TypeError: Cannot read property 'msie' of undefined",
                "timestamp": 1536845291086,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://20tvni1sjxyh352kld2lslvc.wpengine.netdna-cdn.com/wp-content/themes/dt-the7/js/main.min.js?ver=3.8.1 6:27357 Uncaught TypeError: Cannot read property 'webkit' of undefined",
                "timestamp": 1536845291271,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://20tvni1sjxyh352kld2lslvc.wpengine.netdna-cdn.com/wp-content/plugins/convertplug/modules/info_bar/assets/js/info_bar.min.js?ver=4.8.7 0:8510 Uncaught TypeError: Cannot read property 'mozilla' of undefined",
                "timestamp": 1536845297251,
                "type": ""
            }
        ],
        "screenShotFile": "00d20004-004e-0089-00a8-00a9001200e7.png",
        "timestamp": 1536845282042,
        "duration": 26199
    },
    {
        "description": "Angular non js automation test pratice|Just testing non angular app",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "70f1100a5b96c7f7bf5a6f1f1a43167a",
        "instanceId": 9856,
        "browser": {
            "name": "chrome",
            "version": "68.0.3440.106"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://code.jquery.com/jquery-1.10.2.js 8671 Synchronous XMLHttpRequest on the main thread is deprecated because of its detrimental effects to the end user's experience. For more help, check https://xhr.spec.whatwg.org/.",
                "timestamp": 1536845385033,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://20tvni1sjxyh352kld2lslvc.wpengine.netdna-cdn.com/wp-content/themes/dt-the7/js/above-the-fold.min.js?ver=3.8.1 0:10423 Uncaught TypeError: Cannot read property 'msie' of undefined",
                "timestamp": 1536845385882,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://20tvni1sjxyh352kld2lslvc.wpengine.netdna-cdn.com/wp-content/themes/dt-the7/js/main.min.js?ver=3.8.1 6:27357 Uncaught TypeError: Cannot read property 'webkit' of undefined",
                "timestamp": 1536845386060,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://20tvni1sjxyh352kld2lslvc.wpengine.netdna-cdn.com/wp-content/plugins/convertplug/modules/info_bar/assets/js/info_bar.min.js?ver=4.8.7 0:8510 Uncaught TypeError: Cannot read property 'mozilla' of undefined",
                "timestamp": 1536845391384,
                "type": ""
            }
        ],
        "screenShotFile": "004300fe-0045-008d-0048-005000160004.png",
        "timestamp": 1536845378222,
        "duration": 16397
    },
    {
        "description": "Angular non js automation test pratice|Just testing non angular app",
        "passed": true,
        "pending": false,
        "os": "Windows NT",
        "sessionId": "0e464be74f9e61365073e4fbfeea6f18",
        "instanceId": 800,
        "browser": {
            "name": "chrome",
            "version": "68.0.3440.106"
        },
        "message": "Passed",
        "browserLogs": [
            {
                "level": "WARNING",
                "message": "http://code.jquery.com/jquery-1.10.2.js 8671 Synchronous XMLHttpRequest on the main thread is deprecated because of its detrimental effects to the end user's experience. For more help, check https://xhr.spec.whatwg.org/.",
                "timestamp": 1536846725995,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://20tvni1sjxyh352kld2lslvc.wpengine.netdna-cdn.com/wp-content/themes/dt-the7/js/above-the-fold.min.js?ver=3.8.1 0:10423 Uncaught TypeError: Cannot read property 'msie' of undefined",
                "timestamp": 1536846726528,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://20tvni1sjxyh352kld2lslvc.wpengine.netdna-cdn.com/wp-content/themes/dt-the7/js/main.min.js?ver=3.8.1 6:27357 Uncaught TypeError: Cannot read property 'webkit' of undefined",
                "timestamp": 1536846726648,
                "type": ""
            },
            {
                "level": "SEVERE",
                "message": "http://20tvni1sjxyh352kld2lslvc.wpengine.netdna-cdn.com/wp-content/plugins/convertplug/modules/info_bar/assets/js/info_bar.min.js?ver=4.8.7 0:8510 Uncaught TypeError: Cannot read property 'mozilla' of undefined",
                "timestamp": 1536846730385,
                "type": ""
            }
        ],
        "screenShotFile": "00460086-0082-00c8-0027-0010002c0031.png",
        "timestamp": 1536846719633,
        "duration": 11635
    }
];

    this.sortSpecs = function () {
        this.results = results.sort(function sortFunction(a, b) {
    if (a.sessionId < b.sessionId) return -1;else if (a.sessionId > b.sessionId) return 1;

    if (a.timestamp < b.timestamp) return -1;else if (a.timestamp > b.timestamp) return 1;

    return 0;
});
    };

    this.sortSpecs();
});

app.filter('bySearchSettings', function () {
    return function (items, searchSettings) {
        var filtered = [];
        var prevItem = null;

        for (var i = 0; i < items.length; i++) {
            var item = items[i];
            item.displaySpecName = false;

            countLogMessages(item);

            var hasLog = searchSettings.withLog && item.browserLogs && item.browserLogs.length > 0;
            if (searchSettings.description === '' ||
                (item.description && item.description.toLowerCase().indexOf(searchSettings.description.toLowerCase()) > -1)) {

                if (searchSettings.passed && item.passed || hasLog) {
                    checkIfShouldDisplaySpecName(prevItem, item);
                    filtered.push(item);
                    prevItem = item;
                } else if (searchSettings.failed && !item.passed && !item.pending || hasLog) {
                    checkIfShouldDisplaySpecName(prevItem, item);
                    filtered.push(item);
                    prevItem = item;
                } else if (searchSettings.pending && item.pending || hasLog) {
                    checkIfShouldDisplaySpecName(prevItem, item);
                    filtered.push(item);
                    prevItem = item;
                }

            }
        }

        return filtered;
    };
});

var isValueAnArray = function (val) {
    return Array.isArray(val);
};

var checkIfShouldDisplaySpecName = function (prevItem, item) {
    if (!prevItem) {
        item.displaySpecName = true;
        return;
    }

    if (getSpec(item.description) != getSpec(prevItem.description)) {
        item.displaySpecName = true;
        return;
    }
};

var getSpec = function (str) {
    var describes = str.split('|');
    return describes[describes.length - 1];
};

var countLogMessages = function (item) {
    if ((!item.logWarnings || !item.logErrors) && item.browserLogs && item.browserLogs.length > 0) {
        item.logWarnings = 0;
        item.logErrors = 0;
        for (var logNumber = 0; logNumber < item.browserLogs.length; logNumber++) {
            var logEntry = item.browserLogs[logNumber];
            if (logEntry.level === 'SEVERE') {
                item.logErrors++;
            }
            if (logEntry.level === 'WARNING') {
                item.logWarnings++;
            }
        }
    }
};
