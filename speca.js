describe('Testing juliemr.github.io',function()
{
    var first=element(by.model('first'));
    var second=element(by.model('second'));
    var go=element(by.id('gobutton'));
    var result=element(by.className('ng-binding'));
    // It will be executed before every it block
    beforeEach(function()
    {
     browser.get('http://juliemr.github.io/protractor-demo');
    });
    it('title verify',function()
    {
        expect(browser.getTitle()).toEqual('Super Calculator');
    });

    it('addition',function()
    {
       first.sendKeys(2);
       second.sendKeys(5);
       go.click();
       expect(result.getText()).toEqual('7');
    });
});
  
